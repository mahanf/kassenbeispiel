import unittest
from src.kasse import Kasse, Artikel, Warenkorb

class KassenTest(unittest.TestCase):
    def setUp(self):
        self.kasse1 = Kasse(10, 100)
        self.wk1 = Warenkorb()
        self.artikel1 = Artikel(1234345, 0.75)
        self.artikel2 = Artikel(6543623, 3.75)
        self.wk1.artikel_hinzufuegen(self.artikel1)
        self.wk1.artikel_hinzufuegen(self.artikel1)
        self.wk1.artikel_hinzufuegen(self.artikel2)

    def testAbrechnen(self):
        self.assertEqual(self.kasse1.abrechnen(self.wk1),5.15)