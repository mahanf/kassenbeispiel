class Kasse:
    def __init__(self,nummer,bestand):
        self.__kassennummer = nummer
        self.__bestand = bestand

    def abrechnen(self,warenkorb):
        summe = 0
        for artikel in warenkorb.get_warenkorb():
            summe = summe + artikel.get_preis()
        return summe

class Warenkorb:
    def __init__(self):
        self.__warenkorb = []

    def get_warenkorb(self):
        return self.__warenkorb

    def artikel_hinzufuegen(self,artikel):
        self.__warenkorb.append(artikel)

class Artikel:
    def __init__(self, gtin, preis):
        self.__GTIN = gtin
        self.__preis = preis

    def get_preis(self):
        return self.__preis

    def set_preis(self,preis):
        self.__preis = preis

kasse1 = Kasse(10,100)
wk1 = Warenkorb()
artikel1 = Artikel(1234345,0.75)
artikel2 = Artikel(6543623,3.75)
wk1.artikel_hinzufuegen(artikel1)
wk1.artikel_hinzufuegen(artikel1)
wk1.artikel_hinzufuegen(artikel2)
print(kasse1.abrechnen(wk1))
